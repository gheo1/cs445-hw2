import ProblemFour.ImprovedRandom;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ImprovedRandomTest {
    private ImprovedRandom irandom;

    @Test(expected=Test.None.class)
    public void ImprovedRandomClass_Should_be_able_to_instantiate_with_seed(){
        irandom = new ImprovedRandom(10);
    }
    @Test
    public void getRandomIntZeroFromToZero_should_generate_0(){
        irandom = new ImprovedRandom();
        assertEquals(0,irandom.getRandomIntFromTo(0,0));
    }

    @Test(expected=ImprovedRandom.FromGreaterThanToException.class)
    public void getRandomIntFromTo_should_throw_FromGreaterThanToException_When_from_5_to_1_passed(){
        irandom = new ImprovedRandom();
        irandom.getRandomIntFromTo(5,1);
    }
    @Test
    public void getRandomIntFromTo_should_return_integer_between_from_to(){
        irandom = new ImprovedRandom();
        int randomInt = irandom.getRandomIntFromTo(1,5);
        assertTrue( randomInt >= 1 && randomInt <= 5);
        randomInt = irandom.getRandomIntFromTo(-10,5);
        assertTrue( randomInt >= -10 && randomInt <= 5);

        irandom = new ImprovedRandom(10);
        randomInt = irandom.getRandomIntFromTo(-10,-10);
        assertEquals(-10, randomInt);
    }
}
