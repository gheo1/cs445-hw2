import ProblemFive.ImprovedStringTokenizer;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ImprovedStringTokenizerTest {
    private ImprovedStringTokenizer ist;

    @Test(expected=Test.None.class)
    public void ImprovedStringTokenizer_is_able_to_set_delimeter(){
        ist = new ImprovedStringTokenizer("a,b,c,d",",");
    }

    @Test(expected=Test.None.class)
    public void ImprovedStringTokenizer_is_able_to_set_delimeter_and_delimeterInclusiveFlag(){
        ist = new ImprovedStringTokenizer("a,b,c,d", ",", true);
        ist = new ImprovedStringTokenizer("a,b,c,d", ",", false);
    }

    @Test
    public void getStringAsArrayWithEmptyString_Should_Return_empty_array(){
        ist = new ImprovedStringTokenizer("");
        String[] ret = ist.getStringAsArray();
        assertEquals("[]", Arrays.toString(ret));
    }
    @Test
    public void getStringAsArrayWithOneCharacter_Should_return_array_containing_the_character(){
        ist = new ImprovedStringTokenizer("a");
        String[] ret = ist.getStringAsArray();
        assertEquals("[a]", Arrays.toString(ret));
    }
    @Test
    public void getStringAsArrayWithOneWordString_Should_Return_Array_containing_one_element(){
        ist = new ImprovedStringTokenizer("GyucheonHeo");
        String[] ret = ist.getStringAsArray();
        assertEquals("[GyucheonHeo]", Arrays.toString(ret));
    }
    @Test
    public void getStringAsArrayWithTwoWordString_Should_Return_Array_containing_two_same_elements(){
        ist = new ImprovedStringTokenizer("ImprovedStringTokenizer Awesome");
        String[] ret = ist.getStringAsArray();
        assertEquals("[ImprovedStringTokenizer, Awesome]", Arrays.toString(ret));
    }

    @Test
    public void getStringAsArrayWithMultipleWordStringWithAnyDelimeter_should_return_arrayContainingElementsSplittedByGivenDelimeter(){
        ist = new ImprovedStringTokenizer("Popeyes,Kitchen,Louisiana,New Oreleans",",");
        String[] ret = ist.getStringAsArray();
        assertEquals("[Popeyes, Kitchen, Louisiana, New Oreleans]", Arrays.toString(ret));
    }

    @Test
    public void getStringAsArrayWithMultipleWordWithAnyDelimeterAndTrueFlag_should_return_arrayContainingTokensWithDelimeter(){
        ist = new ImprovedStringTokenizer("Popeyes,Kitchen,Louisiana,New Oreleans", ",", true);
        String[] ret = ist.getStringAsArray();
        assertEquals("[Popeyes, ,, Kitchen, ,, Louisiana, ,, New Oreleans]", Arrays.toString(ret));
        ist = new ImprovedStringTokenizer("Popeyes@Kitchen@Louisiana@New@Oreleans", "@", true);
        ret = ist.getStringAsArray();
        assertEquals("[Popeyes, @, Kitchen, @, Louisiana, @, New, @, Oreleans]", Arrays.toString(ret));
    }
}
