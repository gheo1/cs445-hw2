package ProblemFour;

import java.util.Random;

public class ImprovedRandom extends Random {
    public ImprovedRandom(){};
    public ImprovedRandom(long seed){ super(seed); }


    public int getRandomIntFromTo(int from, int to) {
        if(from > to)
            throw new FromGreaterThanToException();
        return super.nextInt(to-from + 1) + from;
    }

    public static class FromGreaterThanToException extends RuntimeException{
    }
}
