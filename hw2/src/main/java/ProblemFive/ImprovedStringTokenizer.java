package ProblemFive;

import java.util.StringTokenizer;

public class ImprovedStringTokenizer extends StringTokenizer {
    public ImprovedStringTokenizer(String string){ super(string); }
    public ImprovedStringTokenizer(String string, String delim){ super(string, delim); }
    public ImprovedStringTokenizer(String string, String delim, boolean returnDelims){ super(string, delim, returnDelims);}

    public String[] getStringAsArray() {
        String[] singleArray = new String[super.countTokens()];
        int i = 0;
        while(super.hasMoreTokens()){
            singleArray[i] = super.nextToken();
            i++;
        }
        return singleArray;
    }
}
