Spring 2020 CS445 HW2
Gyucheon Heo
A20393055

* File Descriptions
|         Name          | Type |   Description
|   hw2                 |  Dir |   Maven project for source codes
|   Gyucheon-Heo-HW2-1-1|  PDF |   Class Diagram for the problem 1.1
|   Gyucheon-Heo-HW2-1-2|  PDF |   Sequence Diagram for the problem 1.2 
|   Gyucheon-Heo-HW2-1-3|  PDF |   The answers for the problem 3

* Instructions
1. Install openjdk-8-default and maven
$>sudo apt-get update -y && sudo apt-get install openjdk-8-jdk maven git

2. Clone the repository
$>git clone http://bitbucket.org/gheo1/cs445-hw2

3. Change the directory to where pom.xml exists
$>cd cs445-hw2/hw2

4. Install dependencies
$>mvn clean install

5. Run Unit Test
$>mvn test

* Metrics
1. Lines of Code : 52 lines
2. Test Coverage
	2.1 ImprovedRandom : 100%
	2.2 ImprovedStringTokenzier : 100%
3. Cyclometic Complexity 
	3.1 The highest : 2 from ImprovedRandom.getRandomFromTo, ImprovedStringTokenizer.getStringArray
	3.2 The lowest : 1 from a rest of methods
